using EntityRelations.Models;
using Microsoft.EntityFrameworkCore;

namespace EntityRelations.DAL
{
    public class Context : DbContext
    {
        public Context(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<FieldDefinition> FieldDefinitions { get; set; }
        public DbSet<FieldValue> FieldValues { get; set; }
        public DbSet<Flow> Flows { get; set; }
        public DbSet<Form> Forms { get; set; }
        public DbSet<Phase> Phases { get; set; }
        public DbSet<FormInstance> FormInstances { get; set; }
        public DbSet<CandidatePhase> CandidatePhases { get; set; }
    }
}