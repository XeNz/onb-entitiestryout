using System.Collections;
using System.Collections.Generic;
using EntityRelations.Models;

namespace EntityRelations.Controllers
{
    public class FormInstancesDto
    {
        public IEnumerable<FormInstance>  FormInstances { get; set; }
    }
}