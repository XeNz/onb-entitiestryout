﻿using System.Collections.Generic;
using System.Linq;
using EntityRelations.DAL;
using EntityRelations.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EntityRelations.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly Context _context;

        public ValuesController(Context context)
        {
            _context = context;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<FormInstance>> Get()
        {
            // create flows with phases, this is actually seed data and will be the same across env
            
            var phases = new List<Phase>
            {
                new Phase
                {
                    Name = "Aanmaak dossier"
                }
            };
            
            var flows = new List<Flow>
            {
                new Flow
                {
                    Name = "Algemene flow",
                    Phases = phases
                }
            };
            
            _context.Flows.AddRange(flows);
            _context.SaveChanges();
            
            
            // add form with fielddefinitions, Im guessing this will also be the same across env
            var fieldDefinitions = new List<FieldDefinition>
            {
                new FieldDefinition
                {
                    Name = "Employment history",
                    Description = "Employment history",
                    DataType = "string"
                }
            };
            var form = new Form {FieldDefinitions = fieldDefinitions};
            _context.Forms.Add(form);
            
            
            // create candidate
            var candidate = new Candidate {FirstName = "alex", LastName = "Willemsen"};
            _context.Candidates.Add(candidate);
            _context.SaveChanges();
            
            // create CanidatePhases
            var candidatePhases = new List<CandidatePhase>
            {
                new CandidatePhase
                {
                    Candidate = candidate,
                    Phase = phases.First(),
                    AssignedTo = "some person with a certain identifier",
                    Status = PhaseStatus.INPROGRESS,
                }
            };
            
            _context.CandidatePhases.AddRange(candidatePhases);
            _context.SaveChanges();
            
            // instantiate new form instance for candidate
            var formInstance = new FormInstance {Form = form};
            
            candidate.FormInstances = new List<FormInstance>{formInstance};
            _context.Candidates.Update(candidate);
            _context.SaveChanges();

            // fill in the forminstance
            formInstance.FieldValues = new List<FieldValue>
            {
                new FieldValue
                {
                    FieldDefinition = fieldDefinitions.First(),
                    Value = "none yo"
                    
                }
            };
            _context.FormInstances.Update(formInstance);
            _context.SaveChanges();
            
            // try to get form instances with the fields for candidate by id , show all the things
            var formInstancesFromAlex = _context.FormInstances
                .Include(x => x.Form)
                .Include(x => x.FieldValues)
                .Include(x=>x.Candidate)
                .ThenInclude(x=>x.CandidatePhases)
                .Include(x=>x.Candidate)
                .ThenInclude(x=>x.FormInstances)
                .Where(x => x.Candidate.FirstName == "alex")
                .ToList();
            
            return Ok(new FormInstancesDto(){FormInstances = formInstancesFromAlex});
        }
    }
}