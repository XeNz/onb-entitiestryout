using System.Collections.Generic;

namespace EntityRelations.Models
{
    public class Flow
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Phase> Phases { get; set; }

    }
}