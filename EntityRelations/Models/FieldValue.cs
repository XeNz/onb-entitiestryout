namespace EntityRelations.Models
{
    public class FieldValue
    {
        public int Id { get; set; }
        public int FieldDefinitionId { get; set; }
        public string Value { get; set; }
        public FieldDefinition FieldDefinition { get; set; }
    }
}