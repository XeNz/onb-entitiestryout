using System.Collections;
using System.Collections.Generic;

namespace EntityRelations.Models
{
    public class Candidate
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<FormInstance> FormInstances { get; set; }
        public ICollection<CandidatePhase> CandidatePhases { get; set; }
    }
}