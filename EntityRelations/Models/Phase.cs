using System.Collections;
using System.Collections.Generic;

namespace EntityRelations.Models
{
    public class Phase
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}