using System.Collections;
using System.Collections.Generic;

namespace EntityRelations.Models
{
    public class Form
    {
        public int Id { get; set; }
        public ICollection<FieldDefinition> FieldDefinitions { get; set; }
    }
}