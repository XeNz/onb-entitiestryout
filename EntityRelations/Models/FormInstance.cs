using System.Collections;
using System.Collections.Generic;

namespace EntityRelations.Models
{
    public class FormInstance
    {
        public int Id { get; set; }
        public int FormId { get; set; }
        public Form Form { get; set; }
        
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }
        public ICollection<FieldValue> FieldValues { get; set; }
    }
}