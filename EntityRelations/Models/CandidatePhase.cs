namespace EntityRelations.Models
{
    public class CandidatePhase
    {
        public int Id { get; set; }
        public Candidate Candidate { get; set; }
        public int CandidateId { get; set; }
        public Phase Phase { get; set; }
        public int PhaseId { get; set; }
        public string AssignedTo { get; set; }
        public PhaseStatus Status { get; set; }
    }
}