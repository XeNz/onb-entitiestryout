using System.Collections;
using System.Collections.Generic;

namespace EntityRelations.Models
{
    public class FieldDefinition
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DataType { get; set; }
        public string Options { get; set; }
        public string Source { get; set; }
        public string SourceField { get; set; }
        public string GdpName { get; set; }
        public int Order { get; set; }
        public string DefaultValue { get; set; }
        public string Action { get; set; }
        public bool ReadOnly { get; set; }
        
        public virtual ICollection<FieldValue> FieldValues { get; set; }
    }
}